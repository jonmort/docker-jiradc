# Dockerized Atlassian Jira Data Center

* A `Dockerfile` for building a Jira Data Center docker image.
* A `docker-compose.yml` for bringing up a Jira Data Center stack.

# Requirements

* Docker 1.13.0+
* Docker Compose 1.10.0+

# Example Usage

`docker-compose up -d --scale jira=3`

This brings up three Jira Data Center containers, with a shared volume storage and shared postgresql database container, and an haproxy in front of it all.

# Cleanup

`docker-compose kill && docker-compose rm && docker volume rm jiradc_jirashareddata jiradc_postgresqldata`

# Credits

This is based heavily on https://github.com/blacklabelops/jira
