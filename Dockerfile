FROM alpine:3.7
# MAINTAINER Steffen Bleul <sbl@blacklabelops.com>
MAINTAINER Matt Saunders <msaunders@adaptavist.com>

ARG JIRA_VERSION=7.6.2
ARG JIRA_PRODUCT=jira-software
# Language Settings
ARG LANG_LANGUAGE=en
ARG LANG_COUNTRY=US

ENV JIRA_USER=jira \
    JIRA_GROUP=jira \
    JIRA_CONTEXT_PATH=ROOT \
    JIRA_HOME=/var/atlassian/jira \
    JIRA_INSTALL=/opt/jira \
    JIRA_SCRIPTS=/usr/local/share/atlassian \
    DOCKERIZE_VERSION=v0.6.0
ENV JAVA_HOME=$JIRA_INSTALL/jre

ENV PATH=$PATH:$JAVA_HOME/bin \
    LANG=${LANG_LANGUAGE}_${LANG_COUNTRY}.UTF-8

COPY imagescripts ${JIRA_SCRIPTS}

RUN apk add --update bash ca-certificates gzip curl tini xmlstarlet && \
\
    # Install glibc
    export GLIBC_VERSION=2.26-r0 && \
    cd /tmp && curl -SLO https://github.com/andyshinn/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk && \
    apk add --allow-untrusted /tmp/glibc-${GLIBC_VERSION}.apk && \
    cd /tmp && curl -SLO https://github.com/andyshinn/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk && \
    apk add --allow-untrusted /tmp/glibc-bin-${GLIBC_VERSION}.apk && \
    cd /tmp && curl -SLO https://github.com/andyshinn/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-i18n-${GLIBC_VERSION}.apk && \
    apk --allow-untrusted add /tmp/glibc-i18n-${GLIBC_VERSION}.apk && \
    /usr/glibc-compat/bin/localedef -i ${LANG_LANGUAGE}_${LANG_COUNTRY} -f UTF-8 ${LANG_LANGUAGE}_${LANG_COUNTRY}.UTF-8 && \
\
    # Install Jira
    export JIRA_BIN=atlassian-${JIRA_PRODUCT}-${JIRA_VERSION}-x64.bin && \
    mkdir -p ${JIRA_HOME} ${JIRA_INSTALL} && \
    curl -SLo /tmp/jira.bin https://downloads.atlassian.com/software/jira/downloads/${JIRA_BIN} && \
    chmod +x /tmp/jira.bin && \
    /tmp/jira.bin -q -varfile ${JIRA_SCRIPTS}/response.varfile && \
\
    # Add user
    addgroup -g 1000 ${JIRA_GROUP} && \
    adduser -u 1000 -G ${JIRA_GROUP} -h /home/${JIRA_USER} -s /bin/bash -S ${JIRA_USER} && \
\
    # Set permissions
    chown -R ${JIRA_USER}:${JIRA_GROUP} ${JIRA_HOME} ${JIRA_INSTALL} ${JIRA_SCRIPTS} /home/${JIRA_USER} && \
\
    # Install dockerize and gosu
    curl -SLo /tmp/dockerize.tar.gz https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz && \
    tar -C /usr/local/bin -xzvf /tmp/dockerize.tar.gz && \
    rm /tmp/dockerize.tar.gz && \
\
    curl -SLo /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64" && \
    chmod +x /usr/local/bin/gosu && \
\
    # Remove obsolete packages
    apk del ca-certificates gzip && \
\
    # Clean caches and tmps
    rm -rf /var/cache/apk/* /tmp/* /var/log/*

# Image Metadata
LABEL com.blacklabelops.application.jira.version=$JIRA_PRODUCT-$JIRA_VERSION \
    com.blacklabelops.application.jira.userid=$CONTAINER_UID \
    com.blacklabelops.application.jira.groupid=$CONTAINER_GID \
    com.blacklabelops.image.builddate.jira=${BUILD_DATE}

#USER jira # Run as root here to access volumes, drop privs later
WORKDIR ${JIRA_HOME}
VOLUME ["/var/atlassian/jira"]
EXPOSE 8080
ENTRYPOINT ["/sbin/tini","--","/usr/local/share/atlassian/docker-entrypoint.sh"]
CMD ["jira"]
